package fr.cines.pac.vitamrestapi.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import fr.cines.pac.vitamrestapi.config.ConfigProperties;
import fr.cines.pac.vitamrestapi.request.RequestDefault;
import fr.cines.pac.vitamrestapi.request.RequestOntologieInternal;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.http.HttpResponse;

@CrossOrigin
@RestController
@RequestMapping(
        value = "/v0.1",
        produces = { "application/json"}
        )
@Api(value = "Main controller of the REST API" )
public class ApiController extends AbstractApiController {

    public ApiController(ConfigProperties configProperties) {
        super(configProperties);
    }

    @ApiOperation(value = "View a list of all the entities")
    @PostMapping("/{entity}")
    public ResponseEntity<String> findEntities(
            @RequestBody(required = false) RequestDefault requestBody,
            @PathVariable("entity") String entity
    )
            throws URISyntaxException, IOException, InterruptedException {

        HttpResponse response = createAndExecuteRequest(requestBody, "/" + entity );

        return new ResponseEntity(response.body(), HttpStatus.resolve(response.statusCode()));
    }

    @ApiOperation(value = "Get an entity by it's identifier")
    @PostMapping("/{entity}/{id}")
    public ResponseEntity<String> findEntitiesById(
            @RequestBody(required = false) RequestDefault requestBody,
            @PathVariable("entity") String entity,
            @PathVariable("id") String id
    )
            throws URISyntaxException, IOException, InterruptedException {

        HttpResponse response = createAndExecuteRequest(requestBody, "/" + entity + "/" + id );

        return new ResponseEntity(response.body(), HttpStatus.resolve(response.statusCode()));
    }

    @ApiOperation(value = "Get the list of ontologies with Origin = INTERNAL")
    @PostMapping("/ontologies/origin/internal")
    public ResponseEntity<String> findOntologiesInternal(
            @RequestBody(required = false) RequestOntologieInternal requestBody
    ) throws IOException, InterruptedException, URISyntaxException {

        HttpResponse response = createAndExecuteRequest(requestBody,"/ontologies" );

        return new ResponseEntity(response.body(), HttpStatus.resolve(response.statusCode()));
    }
}
