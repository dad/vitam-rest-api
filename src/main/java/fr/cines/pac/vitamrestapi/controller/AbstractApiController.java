package fr.cines.pac.vitamrestapi.controller;

import fr.cines.pac.vitamrestapi.config.ConfigProperties;
import fr.cines.pac.vitamrestapi.request.RequestDefault;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public abstract class AbstractApiController {
    private ConfigProperties configProperties;

    @Autowired
    public AbstractApiController(ConfigProperties configProperties) {
        this.configProperties = configProperties;
    }

    public HttpResponse createAndExecuteRequest(RequestDefault request, String path) throws IOException, InterruptedException, URISyntaxException {

        String contrat_acces = (request.getContrat_acces()==null)?configProperties.getContrat_acces():request.getContrat_acces();
        String tenant_id = (request.getTenant_id()==null)?configProperties.getTenant_id():request.getTenant_id();

        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(new URI(configProperties.getAdmin_ext_url() + "/admin-external/v1" + path ))
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .header("X-Tenant-Id", tenant_id)
                .header("X-Access-Contract-Id", contrat_acces)
                .header("X-Http-Method-Override", "GET")
                .POST(HttpRequest.BodyPublishers.ofString(request.getDsl_request().toString()))
                .build();
        return HttpClient.newBuilder()
                .build()
                .send(httpRequest,HttpResponse.BodyHandlers.ofString());
    }
}
