package fr.cines.pac.vitamrestapi.listener;

import fr.cines.pac.vitamrestapi.config.ConfigProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class StartupApplicationListener {

    private ConfigProperties configProperties;

    @Autowired
    public StartupApplicationListener(ConfigProperties configProperties){
        this.configProperties = configProperties;
    }

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
        System.setProperty("javax.net.ssl.keyStore", configProperties.getKeyStore_path());
        System.setProperty("javax.net.ssl.keyStorePassword", configProperties.getKeyStore_password());
        System.setProperty("javax.net.ssl.trustStore", configProperties.getTrustStore_path());
        System.setProperty("javax.net.ssl.trustStorePassword", configProperties.getTrustStore_password());
    }
}