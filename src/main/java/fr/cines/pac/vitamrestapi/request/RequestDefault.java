package fr.cines.pac.vitamrestapi.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Setter
@Getter
@ApiModel(description =  "Non-required JSON request body, allowing the use of a DSL Request, personalising a custom contract access and tenant id")
public class RequestDefault {
    @ApiModelProperty(value = "Request in the DSL language of Vitam in JSON. See Vitam's documentation : http://www.programmevitam.fr/ressources/DocCourante/raml/externe/dsl.html")
    private JsonNode dsl_request;

    @ApiModelProperty(notes = "Custom contract access")
    private String contrat_acces;

    @ApiModelProperty(notes = "Custom tenant id")
    private String tenant_id;

    private final static String dsl = "{ \"$query\": {}, \"$projection\" : {}}";

    @JsonCreator
    public RequestDefault(
            @JsonProperty("dsl_request") JsonNode dsl_request,
            @JsonProperty("contrat_acces") String contrat_acces,
            @JsonProperty("tenant_id") String tenant_id
    ) throws JsonProcessingException {
        if (!dsl_request.isNull() || !dsl_request.isEmpty()) {
            this.dsl_request = dsl_request;
        } else {
            this.dsl_request = new ObjectMapper().readTree(this.dsl);
        }
        this.contrat_acces = contrat_acces;
        this.tenant_id = tenant_id;
    }

    protected RequestDefault(String dsl, String contrat_acces, String tenant_id) throws JsonProcessingException {
        this(new ObjectMapper().readTree(dsl), contrat_acces, tenant_id);
    }
}
