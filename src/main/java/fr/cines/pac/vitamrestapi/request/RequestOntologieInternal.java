package fr.cines.pac.vitamrestapi.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

public class RequestOntologieInternal extends RequestDefault {

    @JsonIgnore
    private JsonNode dsl_request;

    private final static String dsl  =
            "{" +
            " \"$query\": " +
            "  {" +
            "   \"$eq\": {" +
            "     \"Origin\": \"INTERNAL\"" +
            "   }" +
            "  }," +
            " \"$filter\": {}," +
            " \"$projection\": {\n" +
            "   \"$fields\": {\n" +
            "   \"Identifier\": 1,\n" +
            "   \"SedaField\": 1,\n" +
            "   \"Type\": 1,\n" +
            "   \"Collections\":1\n" +
            "  }\n" +
            " }" +
            "}";

    @JsonCreator
    public RequestOntologieInternal(
            @JsonProperty("contrat_acces") String contrat_acces,
            @JsonProperty("tenant_id") String tenant_id
    ) throws JsonProcessingException {
        super(dsl, contrat_acces, tenant_id);
    }

}
