package fr.cines.pac.vitamrestapi;

import fr.cines.pac.vitamrestapi.config.ConfigProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(ConfigProperties.class)
public class VitamRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(VitamRestApiApplication.class, args);
	}

}
