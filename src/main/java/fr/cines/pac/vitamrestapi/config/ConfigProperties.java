package fr.cines.pac.vitamrestapi.config;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@ConfigurationProperties
@ConstructorBinding
@Getter
public class ConfigProperties {

    private final String admin_ext_url;
    private final String contrat_acces;
    private final String keyStore_password;
    private final String keyStore_path;
    private final String trustStore_password;
    private final String trustStore_path;
    private final String tenant_id;

    public ConfigProperties(
            String admin_ext_url,
            String contrat_acces,
            String keyStore_password,
            String keyStore_path,
            String trustStore_password,
            String trustStore_path,
            String tenant_id) {
        this.admin_ext_url = admin_ext_url;
        this.contrat_acces = contrat_acces;
        this.keyStore_password = keyStore_password;
        this.keyStore_path = keyStore_path;
        this.trustStore_password = trustStore_password;
        this.trustStore_path = trustStore_path;
        this.tenant_id = tenant_id;
    }
}
